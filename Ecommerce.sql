-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 12, 2020 at 01:33 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_image`, `user_id`, `ordering`, `status`, `created_at`, `updated_at`) VALUES
(24, 'fd', '1604987829shirt2.jpeg.jpeg', 4, 5, 0, '2020-11-10 00:27:09', '2020-11-10 00:27:09'),
(25, 'sdfd', '1604988005shirt1.jpeg', 4, 7, 0, '2020-11-10 00:30:06', '2020-11-10 00:30:06'),
(28, 'fgfd', '1604988005shirt1.jpeg', 5, 6, 0, NULL, NULL),
(29, 'fgfd', '1604988005shirt1.jpeg', 5, 6, 0, NULL, NULL),
(30, 'fgh3rtor', '1605008893-rose3.jpeg', 4, 333, 0, '2020-11-10 05:08:20', '2020-11-10 06:18:13'),
(31, 'hgjgh1111tt', '1605004746-s7.png', 4, 26, 0, '2020-11-10 05:09:08', '2020-11-10 08:42:23'),
(32, 'fhfg4', '1605009354-rose2.jpg', 2, 54, 1, '2020-11-10 06:25:41', '2020-11-10 06:25:55'),
(33, 'sdfds', '1605015058-rose1.jpeg', 4, 33, 1, '2020-11-10 08:00:58', '2020-11-10 08:00:58'),
(34, 'dsfsdfsdfsdf', '1605070386-s77', 4, 4444444, 1, '2020-11-10 23:23:06', '2020-11-10 23:23:06'),
(35, 'vehical', '1605155983-neck2.png', 6, 4, 1, '2020-11-11 23:09:43', '2020-11-11 23:09:43');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `imagies`
--

CREATE TABLE `imagies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imagies`
--

INSERT INTO `imagies` (`id`, `image_name`, `product_id`, `status`, `created_at`, `updated_at`) VALUES
(1, '1605075064-neck1.jpg', 41, 0, '2020-11-11 00:41:04', '2020-11-11 00:41:04'),
(2, '1605075222-neck1.jpg', 42, 0, '2020-11-11 00:43:42', '2020-11-11 00:43:43'),
(3, '1605075806-ring1.jpg|1605075807-neck2.png|1605075807-neck1.jpg', 45, 0, NULL, NULL),
(4, '1605076193-neck1.jpg', 47, 0, '2020-11-11 00:59:53', '2020-11-11 00:59:54'),
(5, '1605076405-neck1.jpg', 52, 0, '2020-11-11 01:03:25', '2020-11-11 01:03:25'),
(6, '1605078301-ear1.jpg', 62, 0, '2020-11-11 01:35:01', '2020-11-11 01:35:01'),
(7, '1605078896-ear2.jpeg', 67, 0, NULL, '2020-11-11 07:25:55'),
(8, '1605078896-ear1.jpg', 67, 1, NULL, NULL),
(9, '1605078968-ear2.jpeg', 68, 1, NULL, NULL),
(10, '1605078968-ear1.jpg', 68, 0, NULL, NULL),
(11, '1605079058-s77', 69, 0, NULL, '2020-11-11 08:18:27'),
(12, '1605079058-s71', 69, 1, NULL, '2020-11-11 08:18:27'),
(13, '1605079306-shirt3.jpeg', 70, 0, NULL, '2020-11-11 08:11:09'),
(14, '1605079306-shirt2.jpeg', 70, 1, NULL, NULL),
(15, '1605079306-shirt1.jpeg', 70, 0, NULL, NULL),
(16, '1605103576-shirt1.jpeg', 71, 1, NULL, '2020-11-11 08:37:58'),
(17, '1605103576-jeans3.png', 71, 0, NULL, NULL),
(18, '1605154080-images-158462487724258600 (1).jpeg', 72, 1, NULL, NULL),
(19, '1605154080-images-158462487724258600.jpeg', 72, 0, NULL, NULL),
(20, '1605154478-neck1.jpg', 73, 1, NULL, NULL),
(21, '1605154479-ear2.jpeg', 73, 0, NULL, NULL),
(22, '1605154479-ear1.jpg', 73, 0, NULL, NULL),
(23, '1605156020-ring1.jpg', 74, 1, NULL, '2020-11-12 00:20:50'),
(24, '1605156020-neck2.png', 74, 0, NULL, '2020-11-12 00:20:05'),
(25, '1605156020-neck1.jpg', 74, 0, NULL, '2020-11-12 00:20:50'),
(26, '1605156194-rose2.jpg', 75, 0, NULL, '2020-11-12 00:33:28'),
(27, '1605156194-rose1.jpeg', 75, 0, NULL, NULL),
(28, '1605160695-rose3.jpeg', 26, 1, NULL, NULL),
(29, '1605160834-rose3.jpeg', 75, 1, NULL, '2020-11-12 00:33:28'),
(30, '1605160958-rose2.jpg', 75, 0, NULL, NULL),
(31, '1605160979-shirt1.jpeg', 75, 0, NULL, NULL),
(32, '1605165114-shirt2.jpeg', 76, 1, NULL, NULL),
(33, '1605165114-shirt1.jpeg', 76, 0, NULL, NULL),
(34, '1605165218-shirt2.jpeg', 77, 0, NULL, '2020-11-12 05:53:55'),
(35, '1605165218-shirt1.jpeg', 77, 1, NULL, '2020-11-12 05:53:55'),
(36, '1605182222-shirt1.jpeg', 78, 1, NULL, NULL),
(37, '1605182222-jeans3.png', 78, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_05_090235_create_user_details_table', 1),
(5, '2020_11_05_124636_create_categories_table', 2),
(6, '2020_11_07_082211_create_product_table', 3),
(7, '2020_11_07_091152_create_products_table', 4),
(8, '2020_11_07_094120_create_products_table', 5),
(9, '2020_11_11_033529_create_imagies_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `sale_price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `category_id`, `product_code`, `price`, `sale_price`, `quantity`, `product_order`, `status`, `created_at`, `updated_at`) VALUES
(1, 'dss', 24, 'sdfsd4ewe34', 3434, 333, 33, 33, 1, '2020-11-07 04:12:22', '2020-11-07 04:12:22'),
(2, 'dfd', 24, 'dd3e', 333, 33, 7, 6, 1, '2020-11-10 08:56:42', '2020-11-10 08:56:42'),
(3, 'fdgd', 25, 'dfg4dd', 344, 333, 3, 3, 1, '2020-11-10 22:36:19', '2020-11-10 22:36:19'),
(4, 'dfds', 1, 'sde33', 455, 400, 9, 6, 1, '2020-11-10 22:38:29', '2020-11-10 22:38:29'),
(5, 'dfds', 1, 'sde33', 455, 400, 9, 6, 1, '2020-11-10 22:38:53', '2020-11-10 22:38:53'),
(6, 'dfds', 1, 'sde33', 455, 400, 9, 6, 1, '2020-11-10 22:41:42', '2020-11-10 22:41:42'),
(7, 'dfds', 1, 'sde33', 455, 400, 9, 6, 1, '2020-11-10 22:42:15', '2020-11-10 22:42:15'),
(8, 'dfds', 1, 'sde33', 455, 400, 9, 6, 1, '2020-11-10 22:42:40', '2020-11-10 22:42:40'),
(9, 'dfds', 1, 'sde33', 455, 400, 9, 6, 1, '2020-11-10 22:48:15', '2020-11-10 22:48:15'),
(10, 'dfds', 1, 'sde33', 455, 400, 9, 6, 1, '2020-11-10 22:48:33', '2020-11-10 22:48:33'),
(11, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 22:54:18', '2020-11-10 22:54:18'),
(12, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 23:03:42', '2020-11-10 23:03:42'),
(13, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 23:03:56', '2020-11-10 23:03:56'),
(14, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 23:05:48', '2020-11-10 23:05:48'),
(15, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 23:06:39', '2020-11-10 23:06:39'),
(16, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 23:08:10', '2020-11-10 23:08:10'),
(17, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 23:08:29', '2020-11-10 23:08:29'),
(18, 'df', 1, 'sd', 33, 33, 4, 3, 1, '2020-11-10 23:08:39', '2020-11-10 23:08:39'),
(19, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-10 23:25:53', '2020-11-10 23:25:53'),
(20, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:00:59', '2020-11-11 00:00:59'),
(21, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:03:10', '2020-11-11 00:03:10'),
(22, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:03:31', '2020-11-11 00:03:31'),
(23, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:04:50', '2020-11-11 00:04:50'),
(24, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:06:22', '2020-11-11 00:06:22'),
(25, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:10:03', '2020-11-11 00:10:03'),
(26, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:11:27', '2020-11-11 00:11:27'),
(27, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:12:19', '2020-11-11 00:12:19'),
(28, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:13:06', '2020-11-11 00:13:06'),
(29, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:22:37', '2020-11-11 00:22:37'),
(30, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:25:06', '2020-11-11 00:25:06'),
(31, 'dxfd', 1, 'dsf', 66, 55, 4, 5, 1, '2020-11-11 00:26:40', '2020-11-11 00:26:40'),
(32, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:31:11', '2020-11-11 00:31:11'),
(33, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:31:37', '2020-11-11 00:31:37'),
(34, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:32:25', '2020-11-11 00:32:25'),
(35, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:33:39', '2020-11-11 00:33:39'),
(36, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:36:23', '2020-11-11 00:36:23'),
(37, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:37:15', '2020-11-11 00:37:15'),
(38, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:37:32', '2020-11-11 00:37:32'),
(39, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:38:37', '2020-11-11 00:38:37'),
(40, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:40:35', '2020-11-11 00:40:35'),
(41, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:41:03', '2020-11-11 00:41:03'),
(42, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:43:41', '2020-11-11 00:43:41'),
(43, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:51:50', '2020-11-11 00:51:50'),
(44, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:52:11', '2020-11-11 00:52:11'),
(45, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:53:26', '2020-11-11 00:53:26'),
(46, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:59:13', '2020-11-11 00:59:13'),
(47, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 00:59:53', '2020-11-11 00:59:53'),
(48, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:00:51', '2020-11-11 01:00:51'),
(49, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:01:33', '2020-11-11 01:01:33'),
(50, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:01:52', '2020-11-11 01:01:52'),
(51, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:03:17', '2020-11-11 01:03:17'),
(52, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:03:24', '2020-11-11 01:03:24'),
(53, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:18:10', '2020-11-11 01:18:10'),
(54, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:26:12', '2020-11-11 01:26:12'),
(55, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:26:37', '2020-11-11 01:26:37'),
(56, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:29:51', '2020-11-11 01:29:51'),
(57, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:30:33', '2020-11-11 01:30:33'),
(58, 'dfas', 1, 'sdfsd4ewe34', 33, 33, 33, 33, 1, '2020-11-11 01:30:37', '2020-11-11 01:30:37'),
(59, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:32:16', '2020-11-11 01:32:16'),
(60, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:34:02', '2020-11-11 01:34:02'),
(61, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:34:12', '2020-11-11 01:34:12'),
(62, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:35:01', '2020-11-11 01:35:01'),
(63, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:40:35', '2020-11-11 01:40:35'),
(64, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:40:52', '2020-11-11 01:40:52'),
(65, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:42:13', '2020-11-11 01:42:13'),
(66, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 1, '2020-11-11 01:42:28', '2020-11-11 01:42:28'),
(67, 'dfgdeeee', 1, 'dfgr4ee', 4443, 3332, 52, 62, 0, '2020-11-11 01:44:56', '2020-11-11 07:26:58'),
(68, 'dfgd', 1, 'dfgr4', 444, 333, 5, 6, 0, '2020-11-11 01:46:08', '2020-11-11 08:39:02'),
(69, 'dfgdfg', 1, '1', 2, 2, 2, 2, 0, '2020-11-11 01:47:38', '2020-11-11 08:18:27'),
(70, 'dfgdfgfgfg', 1, '1d', 24, 2, 2, 2, 0, '2020-11-11 01:51:46', '2020-11-11 08:11:09'),
(71, 'rttr', 1, 'rr', 44, 3, 3, 34, 0, '2020-11-11 08:36:16', '2020-11-11 08:42:57'),
(72, 'hhh', 29, 'gfh3g', 500, 400, 6, 8, 1, '2020-11-11 22:38:00', '2020-11-11 22:38:00'),
(73, 'sssssds', 30, 'df3de', 454, 343, 2, 5, 1, '2020-11-11 22:44:38', '2020-11-11 22:44:38'),
(75, 'eedddeeee3', 35, '3e3e22', 434355, 34355, 3155, 25, 0, '2020-11-11 23:13:14', '2020-11-12 00:33:28'),
(76, 'dfd', 28, '3d3', 333, 22, 3, 4, 0, '2020-11-12 01:41:54', '2020-11-12 01:41:54'),
(77, 'ffee', 35, 'ff', 444, 33, 33, 3, 1, '2020-11-12 01:43:38', '2020-11-12 05:53:55'),
(78, 'fg', 35, 'rr', 5, 33, 22, 2, 0, '2020-11-12 06:27:02', '2020-11-12 06:27:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dfsd', 'r@gmail.com', NULL, '$2y$10$5olsbMsfaEiEJjoLmg1wweEYPu0.QBBRCK6gdhlmfcq/ls4IJ1tC.', NULL, '2020-11-05 04:20:44', '2020-11-05 04:20:44'),
(2, 'dfgdf', 'llll@gmail.com', NULL, '$2y$10$4EjnNO.u5PUMa7967Xd13ODym81WRzdJ0EksH2rcBPEop5xQ31yA.', NULL, '2020-11-05 06:11:29', '2020-11-05 06:11:29'),
(3, 'ghfg', 'llll12@gmail.com', NULL, '$2y$10$TJznyxFNzuenjPvxgpxt2e0qVOZnpMUNTqZQvtw/YnEloQFQBKBt2', NULL, '2020-11-05 06:16:47', '2020-11-05 06:16:47'),
(4, 'sdfs', 'e@gmail.com', NULL, '$2y$10$5fpYsTu/QgSZMzbJSo2NUOwwiqgojToNdzgYpKP6WzWp0R5A8ZCea', NULL, '2020-11-05 06:22:50', '2020-11-05 06:22:50'),
(5, 'ramesh', 'ramesh12@gmail.com', NULL, '$2y$10$xUrFTGQLzxTPA9ZgQUsGxOhWn.iQOp7LTSZuXhrki7qaDjm4cW4Du', NULL, '2020-11-05 07:09:36', '2020-11-05 07:09:36'),
(6, 'hitesh', 'h@gmail.com', NULL, '$2y$10$rdRSV1POoMmAsYb05YTqperJSJ3Sly6Uz4oVa1Pem6fdM1Hsh6Jt.', NULL, '2020-11-11 23:08:30', '2020-11-11 23:08:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagies`
--
ALTER TABLE `imagies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imagies`
--
ALTER TABLE `imagies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
