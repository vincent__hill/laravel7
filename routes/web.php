<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
// */
Route::get('/', function () {
    return view('login');
});
Route::get('/login', function () {
    return view('/login');
})->name('login');
// Route::redirect('logout', 'login', 301);
Route::get('/logout', 'Project@logout')->name('login');
Route::post('/login-check', 'Project@authenticate');
Route::get('/registration', function () {
    return view('registration');
});
Route::post('/registered', 'Project@storeUser');

// Route::group(['prefix' => 'admin'], function () {
//     Route::get('index', function () {
//         // Matches The "/admin/users" URL
//     })->name('admin.index');
//     // Route::view('/index', 'index')->name('admin.index');
// });
Route::prefix('admin')->group(function () {
    Route::get('index', function () {
        // Matches The "/admin/users" URL
    });
});
Route::middleware(['auth'])->group(function () {
    // Route::view('/index', 'index');
    Route::post('/sorting', 'CategoryController@getSorting')->name('sorting.post');
    Route::resource('categories', 'CategoryController', [
        'except' => ['show']]);
    Route::post('/productSorting', 'ProductController@getSorting')->name('productSorting.post');
    Route::resource('products', 'ProductController', [
        'except' => ['show']]);
});

// Route::middleware(['auth'])->group(function(){
//     Route::view('/index','index');
//     Route::get('category-add',array('as' => 'category-add', function(){
//         return view('Category.category-add');
//     }));
//     // Route::get('category-add','Project@editCategory');
//     Route::get('product-add','ProductController@show');
//     Route::get('category-edit/{id}','CategoryController@editCategory');
//     Route::Post('category-edited/{id}','CategoryController@updateCategory');
//     Route::get('category-delete/{id}','CategoryController@categoryDelete');
//     Route::post('category','CategoryController@storeCategory');
//     Route::get('category-list','CategoryController@getCategory');
//     Route::post('/sorting','CategoryController@getSorting')->name('sorting.post');
//     Route::get('product-list','ProductController@getProduct');
//     Route::get('product-edit/{id}','ProductController@editProduct');
//     Route::Post('product-edited/{id}','ProductController@updateProduct');
//     Route::get('product-delete/{id}','ProductController@productDelete');
//     // Route::get('product-add',array('as' => 'product-add', function(){
//     //     return view('Product.product-add');
//     // }));
//     Route::post('product', 'ProductController@storeProduct');
//     // Route::('category-edit/{id}','Project@editCategory');
//     Route::post('/productSorting','ProductController@productSorting')->name('productSorting.post');
// });

// Route::get('category-list',array('as' => 'category-list', function(){
//     return view('Category.category-list');
// }));
// Route::view('category-list', 'category.category');
// Route::get('category.category-list')
