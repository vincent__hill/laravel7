<?php
    namespace App\Helpers;
    use Illuminate\Auth\Events\Validated;
    // use Illuminate\Http\Request;
	use GuzzleHttp\Psr7\Request;
	use Intervention\Image\Facades\Image;

class Helper{
        // public function imageUpload($files,$path,$uploadThumb){
        //     global $constant;
		// 	$new_width=100;
		// 	$new_height=100;
		// 	$new_width1=1000;
		// 	$new_height1=700;
		// 	$target_file=$path.basename($files);
		// 	$check=getimagesize($files);
		// 	//print_r($check);
		// 	list($width,$height)=getimagesize($files);
        //     $imageFileType=$files->extension();
        //     $imageName = time().'.'.$files->getClientOriginalName();
		// 	if($imageFileType!= "jpg" && $imageFileType!="jpeg" && $imageFileType!="png"){
		// 		echo "sorry Only jpg, png and jpeg allow";
        //     }
        //     $files->move(base_path($path), $imageName);
		// 			echo $path.$imageName;
		// 		Image::make($path.$imageName)->resize(320, 240)->save('/public/storage/uploads/Category/thumb/'.$imageName);
		// 	exit;
        //     // $files->move(base_path($uploadThumb), $imageName);
        //     return ($imageName);

		// 	// if($width>1024){
        //     //     $files->move(base_path($path), $imageName);
        //     //     switch($imageFileType){
		// 	// 	case "jpeg":
		// 	// 		$OldImage=imagecreatefromjpeg($path,$imageName);
		// 	// 	break;
		// 	// 	case "jpg":
		// 	// 		$OldImage=imagecreatefromjpeg($path,$imageName);
		// 	// 	break;
		// 	// 	case "png":
		// 	// 		$OldImage=imagecreatefrompng($path,$imageName);
		// 	// 	break;
		// 	// 	default:
		// 	// 		$OldImage=imagecreatefromjpeg($path,$imageName);
		// 	// }
		// 	// $NewImage1=imagecreatetruecolor($new_width1,$new_height1);
		// 	// imagecopyresized($NewImage1,$OldImage,0,0,0,0,$new_width1,$new_height1,$width,$height);
		// 	// switch($imageFileType){
		// 	// 	case "jpeg":
		// 	// 		  imagejpeg($NewImage1,$path.$imageName);
		// 	// 	break;
		// 	// 	case "png":
		// 	// 		 imagepng($NewImage1,$path.$imageName);
		// 	// 	break;
		// 	// 	case "jpg";
		// 	// 		 imagejpeg($NewImage1,$path.$imageName);
		// 	// 	break;
		// 	// 	default:
		// 	// 		 imagejpeg($NewImage1,$path.$imageName);

		// 	// }
		// 	// }else{
        //     //     $files->move(base_path($path), $imageName);
		// 	// }
		// // 	switch($imageFileType){
		// // 		case "jpeg":
		// // 			$OldImage=imagecreatefromjpeg($path.$files['name'].time());
		// // 		break;
		// // 		case "jpg":
		// // 			$OldImage=imagecreatefromjpeg($path.$files['name'].time());
		// // 		break;
		// // 		case "png":
		// // 			$OldImage=imagecreatefrompng($path.$files['name'].time());
		// // 		break;
		// // 		default:
		// // 			$OldImage=imagecreatefromjpeg($path.$files['name'].time());
		// // 	}
		// // 	$NewImage=imagecreatetruecolor($new_width,$new_height);
		// // 	imagecopyresized($NewImage,$OldImage,0,0,0,0,$new_width,$new_height,$width,$height);
		// // 	switch($imageFileType){
		// // 		case "jpeg":
		// // 			  imagejpeg($NewImage,$uploadThumb.$files['name'].time());
		// // 		break;
		// // 		case "png":
		// // 			 imagepng($NewImage,$uploadThumb.$files['name'].time());
		// // 		break;
		// // 		case "jpg";
		// // 			 imagejpeg($NewImage,$uploadThumb.$files['name'].time());
		// // 		break;
		// // 		default:
		// // 			 imagejpeg($NewImage,$uploadThumb.$files['name'].time());
		// // 	}
		// // 	return($imageFileType);
        // // }
        public function imageUpload($filename, $path,$thumbPath){
            
			$imageName = time().'-'.$filename->getClientOriginalName();
			$data=getimagesize($filename);
			// echo $width = $data[0];
			// echo "<br>";
			// echo $height = $data[1];
			// exit;
		$filename->move(public_path($path), $imageName);
		$thumbnailPath=public_path($path.$imageName);
		//echo public_path('/storage/uploads/Category/'.$imageName);
		if($data[0] >= 1024 && $data[1] >=720){
			Image::make($thumbnailPath)->resize(900,800)->save($thumbnailPath);
		}
		Image::make($thumbnailPath)->resize(100,100)->save(public_path($thumbPath.$imageName));
        	return ($imageName);
        }

    }
        
?>