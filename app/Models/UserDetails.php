<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;

class UserDetails extends Authenticatable
{
    //
    protected $table="userDetails";
    protected $fillable = [
        'vUserName', 'vEmail' , 'password', 'dDob'
    ];
}
