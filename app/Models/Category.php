<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table="categories";
    protected $fillable = [
        'category_name', 'category_image' , 'user_id', 'ordering','status'
    ];
    // public function  user(){
    //     return  $this->hasMany('App\Models\User','user_id');
    // }
    public function products(){
        return $this->hasMany('App\Models\Product','category_id');
    }
}
