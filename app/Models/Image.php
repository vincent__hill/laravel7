<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $table="images";
    protected $fillable = [
        'image_name',
        'product_id' ,
        'status'
    ];
    public function products(){
        return $this->belongsTo('App\Models\Product','id');
    }
}

