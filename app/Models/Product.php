<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table="products";
    //  protected $id=$table->id;
    protected $fillable = [
        'product_name',
        'category_id' ,
        'product_code',
        'price',
        'sale_price',
        'quantity',
        'product_order',
        'status'
    ];
    public function category(){
        return $this->belongsTo('App\Models\Category','category_id');
    }
    public function image(){
        return $this->hasMany('App\Models\Image','product_id');
    }
}
