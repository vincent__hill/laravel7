<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Models\UserDetails;

class AuthController extends Controller
{
    public function register()
    {

      return view('auth.register');
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'vUserName' => 'required|string|max:255',
            'vEmail' => 'required|string|email|max:255|unique:users',
            'vPassword' => 'required|string|min:8|confirmed',
            'dDob' => 'required'
        ]);

        UserDetails::create([
            'vUserName' => $request->vUserName,
            'vEmail' => $request->vEmail,
            'dDob' => $request->dDob,
            'vPassword' => bcrypt($request->vPassword),
        ]);

        return redirect('home');
    }

    public function login()
    {

      return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'vEmail' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('vEmail', 'vPassword');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('home');
        }

        return redirect('login')->with('error', 'Oppes! You have entered invalid credentials');
    }

    public function logout() {
      Auth::logout();

      return redirect('login');
    }

    public function home()
    {
      return view('home');
    }
}
