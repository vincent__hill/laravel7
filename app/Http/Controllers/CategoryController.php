<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Category;
use File;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $category = Category::with('products')
            ->where('user_id', auth()->user()->id)
            ->orderBy('ordering')
            ->get();
        return view('category.index', ['values' => $category]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $category = new Category();
        $path = '/storage/uploads/category/';
        $thumbPath = '/storage/uploads/category/thumb/';
        $helper = new Helper();
        $image = $request->category_image;
        $imageName = $helper->imageUpload($image, $path, $thumbPath);
        $category->category_name = $request->category_name;
        $category->category_image = $imageName;
        $category->user_id = auth()->user()->id;
        $category->ordering = $request->ordering;
        $category->status = $request->status;
        $insert = $category->save();
        //dd($insert);
        if (!$insert) {
            return ("not inserted");
        } else {
            return redirect()->route('categories.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return ("show");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // return($id);
        // return  redirect()->route('categories.index');
        $user = Category::find($id);
        // dd($user);
        return view('category.create')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $path = '/public/storage/uploads/category/';
        $thumbPath = '/storage/uploads/category/thumb/';
        $data = new Helper();
        $image = $request->category_image;
        if ($image != null) {
            $imageName = $data->imageUpload($image, $path, $thumbPath);
        }
        if ($image == null) {
            Category::where("id", $id)
                ->update(array(
                    "category_name" => $request->category_name,
                    "ordering" => $request->ordering,
                    "status" => $request->status));
        } else {
            Category::where("id", $id)
                ->update(array(
                    "category_name" => $request->category_name,
                    "category_image" => $imageName,
                    "ordering" => $request->ordering,
                    "status" => $request->status));
        }
        return redirect()->route('categories.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $getData = Category::with('products')->where('id', $id)->first();
        $path = public_path() . '/storage/uploads/category/' . $getData->category_image;
        $thumbPath = public_path() . '/storage/uploads/category/thumb/' . $getData->category_image;
        File::delete($path);
        File::delete($thumbPath);
        Category::where('id', $id)->delete();
        return redirect()->route('categories.index');
    }
    public function getSorting(Request $request)
    {

        $query = Category::with('products')->where('user_id', auth()->user()->id);
        if (isset($request->value) && !empty($request->value)) {
            $query = $query->where(function ($q) use ($request) {
                $q->where('category_name', 'like', '%' . $request->value . '%');
                $q->orWhere('ordering', 'like', '%' . $request->value . '%');
                $q->orWhere('status', 'like', '%' . $request->value . '%');
                $q->orWhere('created_at', 'like', '%' . $request->value . '%');
                $q->orWhere('updated_at', 'like', '%' . $request->value . '%');})
                ->orderBy('ordering');
        }
        if (isset($request->column) && !empty($request->column)) {
            $query = $query->orderBY($request->column, $request->sort);
        }
        $query = $query->groupBy('id')
            ->get();
        return view('category.sorting', ['values' => $query]);
    }
}
