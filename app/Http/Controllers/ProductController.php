<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use File;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // //
        $value = auth()->user()->id;
        $product = Product::with('image', 'category')
            ->whereHas('category', function ($q) use ($value) {
                $q->where('user_id', $value);})
            ->orderBy('product_order')
            ->get();
        return view('product.index', ['values' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $items = Category::where('user_id', auth()->user()->id)->pluck('category_name', 'id');
        return view('product.create', ['values' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $product = new Product();
        $insertImage = new Image();
        $temp = 0;

        $product->product_name = $request->product_name;
        $product->category_id = $request->category_id;
        $product->product_code = $request->product_code;
        $product->price = $request->price;
        $product->sale_price = $request->sale_price;
        $product->quantity = $request->quantity;
        $product->product_order = $request->product_order;
        $product->status = $request->status;
        $product->save();
        $getData = Product::select('*')
            ->orderBy('id', 'DESC')
            ->take(1)
            ->first();
        $images = array();
        if ($files = $request->file('image_name')) {
            $path = '/storage/uploads/product/';
            $thumbPath = '/storage/uploads/product/thumb/';
            foreach ($files as $file) {
                $helper = new Helper();
                $image = $file;
                $imageName = $helper->imageUpload($image, $path, $thumbPath);
                $temp++;
                $images[] = $imageName;
            }
            for ($i = 0; $i < $temp; $i++) {
                //    echo $images[$i];
                if ($i == 0) {
                    $status = 1;
                } else {
                    $status = 0;
                }
                Image::insert([
                    'image_name' => $images[$i],
                    'product_id' => $getData->id,
                    'status' => $status]);
            }
        }

        if (!($product->save())) {
            return ("Data is not inserted");
        } else {
            return redirect()->route('products.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = Product::with('image', 'category')->find($id);
        return view('product.create')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // dd($request->check);
        $path = '/public/storage/uploads/product/';
        $thumbPath = '/storage/uploads/product/thumb/';
        Product::where("id", $id)
            ->update(array(
                "product_name" => $request->product_name,
                "category_id" => $request->category_id,
                "product_code" => $request->product_code,
                "price" => $request->price,
                "sale_price" => $request->sale_price,
                "quantity" => $request->quantity,
                "product_order" => $request->product_order,
                "status" => $request->status));
        if ($request->check != null) {
            $getData = Image::select('*')
                ->where('product_id', $id)
                ->where('status', '1')
                ->first();
            $imageId = $getData['id'];
            $status = $getData->status;
            $data = Image::where("product_id", $id)
                ->where('id', $imageId)
                ->update(array(
                    "status" => '0',
                ));
            $data1 = Image::where('product_id', $id)
                ->where('id', $request->check)
                ->update(array(
                    "status" => '1',
                ));
        }
        if ($request->image_name != null) {
            $temp = 0;
            if ($files = $request->file('image_name')) {
                $path = '/storage/uploads/product/';
                $thumbPath = '/storage/uploads/product/thumb/';
                foreach ($files as $file) {
                    $helper = new Helper();
                    $image = $file;
                    $imageName = $helper->imageUpload($image, $path, $thumbPath);
                    $temp++;
                    $images[] = $imageName;
                }
                for ($i = 0; $i < $temp; $i++) {
                    Image::insert([
                        'image_name' => $images[$i],
                        'product_id' => $id,
                        'status' => '0']);
                }
            }
        }
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $getData = Image::where('product_id', $id)->get();
        foreach ($getData as $value) {
            $path = public_path() . '/storage/uploads/product/' . $value->image_name;
            $thumbPath = public_path() . '/storage/uploads/product/thumb/' . $value->image_name;
            File::delete($path);
            File::delete($thumbPath);
        }
        Image::where('product_id', $id)->delete();
        Product::where('id', $id)->delete();
        return redirect()->route('products.index');
    }
    public function getSorting(Request $request)
    {
        $value = auth()->user()->id;
        // dd($request->selected);
        $query = Product::with('image', 'category')
            ->whereHas('category', function ($q) use ($value) {
                $q->where('user_id', $value);});
        // dd($query);
        // $query=Product::select('*','products.status as productStatus')
        //                  ->leftJoin('imagies',function($join){
        //              $join->on('products.id','=','imagies.product_id');})
        //                  ->leftJoin('categories',function($join){
        //              $join->on('products.category_id','=','categories.id');})
        //                  ->where('categories.user_id',auth()->user()->id)
        //                  ->where('imagies.status','1');
        if (isset($request->value) && !empty($request->value)) {
            $query = $query->where(function ($q) use ($request) {
                $q->where('product_name', 'like', '%' . $request->value . '%');
                $q->orWhere('product_code', 'like', '%' . $request->value . '%');
                $q->orWhere('price', 'like', '%' . $request->value . '%');
                $q->orWhere('created_at', 'like', '%' . $request->value . '%');
                $q->orWhere('updated_at', 'like', '%' . $request->value . '%');
            });
        }
        if (!empty($request->priceFrom)) {
            //    echo  $concate=where('price','>=',$request->priceFrom);
            $query = $query->where('price', '>=', $request->priceFrom);
            //    dd($getData);
        }
        if (!empty($request->priceTo)) {
            $query = $query->where('price', '<=', $request->priceTo);
        }
        if (!empty($request->qty1)) {
            $query = $query->where('quantity', '>=', $request->qty1);
        }
        if (!empty($request->qty2)) {
            $query = $query->where('quantity', '<=', $request->qty2);
        }
        if (is_numeric($request->selected)) {
            $query = $query->where('status', '=', $request->selected);
        }
        $query = $query->orderBY('product_order')->get();
        //  dd($query);
        return view('product.productSorting', ['values' => $query]);
    }
}
