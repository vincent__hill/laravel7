<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('/public/css/bootstrap.min.css')}}">
    <script src="{{asset('/public/js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{asset('/public/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('/public/js/additional-methods.min.js')}}"></script>
    <style>
        .error{
            color:red;
        }
    </style>
    
</head>
<body>
<div class="container py-3 my-5 bg-light border">
        <h1 class="text-center">Registration</h1>
        <form action="registered" method="post" id="registration">
          @csrf
            <div class="form-group px-5">
                <label>
                    Name
                </label>
                <input type="text" name="name" class="form-control" id="name" required>
            </div>
            <div class="form-group px-5">
                <label>Email</label>
                <input type="text" name="email" class="form-control" id="email" required>
            </div>
            <div class="form-group px-5">
                <label>Password</label>
                <input type="password" name="password" class="form-control" id="password" required>
            </div>
            
            <div id="success" class="px-5"></div>
            <div class="form-group px-5">
                <button type="submit" class="btn bg-primary" id="submit" name="submit">Submit</button>
            </div>
        </form>
    </div>
    <script type="text/javascript">

        $("#registration").validate({
              rules: {
                    name: {
                        required:true,
                        maxlength: 20
                     },
                     email: {
                        required: true,
                        email:true
                    },
                    password:{
                        required:true,
                        minlength:6
                    }
              },
              messages:{
                name: {
                  required: "Please Enter Your Name",
                  maxlength: "Please Enter max 20 char"
                },
                email: {
                  required: "Please Enter Email",
                  email:"Please Enter Valid Email"
                },
                password:{
                    required:"Please Enter Password",
                    minlength:"Please Enter minimum 6 Digit"
                }
              }
            });
            </script>
</body>
</html>

