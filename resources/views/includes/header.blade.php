<div class="card-header pt-2 mt-3 bg-light">
    <div class="row">
        <div class="col-sm-4">
            <ul class="nav">
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="/" role="button" aria-haspopup="true" aria-expanded="false">Category</a>
            <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ url('/categories/create')}}">ADD</a>
            <a class="dropdown-item" href="{{ url('/categories')}}">Manage</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Product</a>
            <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ url('/products/create')}}">ADD</a>
            <a class="dropdown-item" href="{{ url('/products')}}">Manage</a>
        </li>
        </ul>
        </div>
        <div class="col-sm-4 text-center">
        <h5>Welcome:{{auth()->user()->name}}</h5>
        </div>
        <div class="col-sm-4">
            <div class="float-right">
            <a href="{{ url('/logout')}}">Logout</a> </div>
        </div>
    </div>
</div>