<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
    @include('includes.script')
</head>
<body>
    @include('includes.header')

    @yield('content')

</body>
</html>