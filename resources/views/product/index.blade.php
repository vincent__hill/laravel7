
{{-- @if (Auth::check()) --}}
{{--  @include('header')  --}}
{{-- @else --}}
{{-- <script type="text/javascript">
     window.location ="{{ url('/login')}}";//here double curly bracket
</script>
@endif --}}
@extends('layouts.layoutPage') 
@section('content')
    
<h1 class="text-center">Product</h1>
<div class="pl-5">
    <form method="post">
        <label class="label">Select Status</label>
        <select id="status" name="status">
            <option value=" ">Select</option>
            <option value="1" id="active">Active</option>
            <option value="0" id="inactive">Inactive</option>
        </select>
    </form>
</div>
<form method="post">
    <div class="px-5 pt-2">
        <input type="number" name="price1" placeholder="Enter Price From..." id="price1">
        <input type="number" name="price2" placeholder="Enter Price To..." id="price2">
        {{-- <input type="button" name="price" value="GO" class="btn btn-success" id="sorting"> --}}
    </div>
</form>
<form method="post">
    <div class="px-5 pt-2">
        <input type="number" name="qty1" placeholder="Enter Quantity From..." id="qty1">
        <input type="number" name="qty2" placeholder="Enter Quantity To..." id="qty2">
        {{-- <input type="button" name="qty" value="GO" class="btn btn-success" id="qtysorting"> --}}
    </div>
</form>
<div class="px-5 pt-2">
<input type="text" name="search" id="search" class="form-control" placeholder="search...">
</div>
<div class="pr-3 my-4 table-responsive" id="data">
    <table class="table table-striped" id="table">
        <thead>
            <tr class="bg-secondary">
                <th>NO</th>
                <th>Image</th>
                <th>Name</th>
                <th>Product Code</th>
                <th>Category Name</th>
                <th>Price/SalePrice</th>
                <th>Quantity</th>
                <th>order</th>
                <th>Added</th>
                <th>modified</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="sort">
            @php
            // print_r($values);
             // exit();
             
             $path='/public/storage/uploads/product/thumb/';
         @endphp
            @if (count($values)>0)
            @foreach ($values as $key=> $value )
                {{-- <pre>
                 {{print_r($values)}}
                 {{}}
                    {{exit}} --}}
              
                <tr>
                    <td>#</td>
                    @foreach ($value->image as $image )
         
    
                         @if ($image->status=='1')
                              <td><img src="{{ asset($path.$image->image_name)}}" alt="Category-Image"></td>
                                    @break;
                        @endif
                    @endforeach
                    <td>{{$value->product_name}}</td>
                    <td>{{$value->product_code}}</td>
                    <td>{{$value->category->category_name}}</td>
                    <td>{{$value->price }} {{'/'}}  {{$value->sale_price }}</td>
                    <td>{{$value->quantity}}</td>
                    <td>{{$value->product_order}}</td>
                    <td>{{$value->created_at}}</td>
                    <td>{{$value->updated_at}}</td>
                    <td>
                        @if ($value->status==1)
                            <button class="btn-success" id="active" >Active</button>
                        @else
                            <button class="btn-warning" id="inactive">Inactive</button>
                        @endif
                    </td>
                    <td>
                        <a href="{{ url('products/'.$value->id.'/edit')}}"><button class="btn">Edit</button></a>
                        <form action="{{ url('products/'.$value->id)}}" method="post">
                            @csrf
                            {{ method_field('delete') }}

                            <button class="btn btn-default" onclick="return confirmDelete()">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            @else
               <tr>
                   <td colspan="12" class="text-center error"> Data is not Available!!</td>
               </tr>
           @endif
        </tbody>

    </table>
</div>
<script>
    $(document).ready(function(){
                $("#search").on("keyup",function(){
                    // alert("ldkflk");
                    var value = $(this).val().toLowerCase();
                    var userId = "{{$user=auth()->user()->id}}";
                    $.ajax({
                        url:'{{route("productSorting.post")}}',
                        type:'POST',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            'value': value,
                            'userId': userId
                        },
                        success:function(data){
                          //  alert(data);
                            console.log(data);
                            $(".table tr:not(:first)").remove();
						    $(".table").append(data);
                    //         if (!data) {
					// 	//$("#msg").text("Data is not Available");
					// 	$(".table tr:not(:first)").remove();
					// } else {
					// 	//$("#msg").text(" ");
					// 	$(".table tr:not(:first)").remove();
					// 	$(".table").append(data);
					// }
                        }
                    });
                });
                $("#price1 ,#price2 ").keyup(function(){
                        var priceFrom=$("#price1").val();
                        var priceTo=$("#price2").val();
                        var userId="{{$user=auth()->user()->id}}";
                        $.ajax({
                                url:'{{route("productSorting.post")}}',
                                type:'post',
                                data:{
                                    "_token": "{{ csrf_token() }}",
                                    "priceFrom":priceFrom,
                                    "priceTo":priceTo,
                                    "userId":userId,
                                    "selected":" ",
                                    "status":" "
                                },
                                success:function(data){
                                    console.log(data);
                                    if(!data){
                                // $("#msg").text("Data is not Available");
                                    $(".table tr:not(:first)").remove();
                                // location.reload();
                                    }else{
                                    //$("#msg").text(" ");
                                    $(".table tr:not(:first)").remove();
                                    $(".table").append(data);
                                    }
                                }


                        });
                 });
                 $("#qty1,#qty2").keyup(function(){
                        var qty1=$("#qty1").val();
                        var qty2=$("#qty2").val();
                        var userId="{{$user=auth()->user()->id}}";
                        $.ajax({
                            url:'{{route("productSorting.post")}}',
                            type:"post",
                            data:{
                                "_token": "{{ csrf_token() }}",
                                "qty1":qty1,
                                "qty2":qty2,
                                "userId":userId,
                                "selected":" ",
                                "status":" "
                            },
                            success:function(data){
                                console.log(data);
                                if(!data){
                                // $("#msg").text("Data is not Available");
                                    $(".table tr:not(:first)").remove();
                                    }else{
                                // $("#msg").text(" ");
                                    $(".table tr:not(:first)").remove();
                                    $(".table").append(data);
                                    }
                            }
                        });
                 });
                 $("#status").click(function(){
                        var selected=$(this).children("option:selected").val();
                        var userId="{{$user=auth()->user()->id}}";
                        $.ajax({
                            url:'{{route("productSorting.post")}}',
                            type:"post",
                            data:{
                                "_token": "{{ csrf_token() }}",
                                "selected":selected,
                                "userId":userId,
                                "status":""
                            },
                            success:function(data){
                                console.log(data);
                                if(!data){
                                    $("#msg").text("Data is not Available");
                                    $(".table tr:not(:first)").remove();
                                    }else{
                                    $("#msg").text(" ");
                                    $(".table tr:not(:first)").remove();
                                    $(".table").append(data);
                                    }
                            }
                        });

                 });
   });
       function confirmDelete() {
		//alert("dsfl;kld");
                    if (confirm("Do You Want Delete ??") == true) {
                        return true;
                    } else {
                        // alert("Cancel by user");
                        return false;
                    }
	            }
</script>
@endsection