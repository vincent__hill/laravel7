@php

 $path='/public/storage/uploads/product/thumb/';
@endphp
{{print_r($values)}}
@if (count($values)>0)
@foreach ($values as $value )
    {{--  {{print_r($values)}}
    {{$value->image->image_name}}  --}}
    <tr>
        <td>#</td>
        @foreach ($value->image as $image )
            @if ($image->status=='1')
             <td><img src="{{ asset($path.$image->image_name)}}" alt="Category-Image"></td>
                   @break;
            @endif
        @endforeach
        <td>{{$value->product_name}}</td>
        <td>{{$value->product_code}}</td>
        <td>{{$value->category->category_name}}</td>
        <td>{{$value->price }} {{'/'}}  {{$value->sale_price }}</td>
        <td>{{$value->quantity}}</td>
        <td>{{$value->product_order}}</td>
        <td>{{$value->created_at}}</td>
        <td>{{$value->updated_at}}</td>
        <td>
            @if ($value->status==1)
                <button class="btn-success" id="active" >Active</button>
            @else
                <button class="btn-warning" id="inactive">Inactive</button>
            @endif
        </td>
        <td>
            <a href="{{ url('products/'.$value->id.'/edit')}}"><button class="btn">Edit</button></a>
                        <form action="{{ url('products/'.$value->id)}}" method="post">
                            @csrf
                            {{ method_field('delete') }}

                            <button class="btn btn-default" onclick="return confirmDelete()">Delete</button>
                        </form>       </td>
    </tr>
@endforeach
@else
   <tr>
       <td colspan="12" class="text-center error"> Data is not Available!!</td>
   </tr>
@endif
