{{-- @if (Auth::check()) --}}
@extends('layouts.layoutPage') 
@section('content')
	{{-- @else
<script type="text/javascript">
     window.location ="{{ url('/login')}}";//here double curly bracket
</script>
@endif --}}
<div class="container py-3 my-5 bg-light border">
<h1 class="text-center">Product</h1>
<form action="{{url(isset($user) ? 'products/'.$user->id : '/products')}}"  method="POST"  enctype="multipart/form-data" id="productForm">
	@csrf
	@if (isset($user))
		@method('PUT')
	@endif
    <div class="form-group px-5">
        <label for="">Name</label>
        <input type="text" class="form-control" name="product_name" value="{{isset($user) ? $user->product_name : ''}}">
    </div>
    <div class="form-group px-5">
        <label for="">Category</label>
		<select class="form-control" name="category_id" required>
			@if (isset($user))
		
				{{-- <option value="{{$user[0]->category_id}}">{{$user->category_name}}</option> --}}
			<option value="{{$user->category_id}}">{{$user->category->category_name}}</option>
			@else
					 @foreach ($values as $id => $category )
					<option value={{$id}}>{{$category}}
					 </option>
					  @endforeach
			@endif
	</select>
    </div>
    <div class="form-group px-5">
        <label for="">Product code</label>
        <input type="text" class="form-control" name="product_code" value="{{isset($user) ? $user->product_code : ''}}">
    </div>
    <div class="form-group px-5">
        <label for="">Price</label>
        <input type="number" class="form-control" name="price" value="{{isset($user) ? $user->price : ''}}">
    </div>
    <div class="form-group px-5">
        <label for="">Sale price</label>
        <input type="number" class="form-control" name="sale_price" value="{{isset($user) ? $user->sale_price : '' }}">
    </div>
    <div class="form-group px-5">
        <label for="">Quantity</label>
        <input type="number" class="form-control" name="quantity" value="{{isset($user) ? $user->quantity : '' }}">
    </div>
    <div class="form-group px-5">
        <label for="">Order</label>
        <input type="number" class="form-control" name="product_order" value="{{isset($user) ? $user->product_order : ''}}">
    </div>
    <div class="form-group px-5">
        <label for="">Status</label>
        <input type="radio"  name="status" value="1" @if (isset($user))
                                                                @if ($user->status==1)
                                                                {{"checked"}}
                                                                @endif
                                                                @endif> Active
                <input type="radio"  name="status" value="0" @if (isset($user))
                                                                @if ($user->status==0)
                                                                {{"checked"}}
                                                                @endif
                                                                 @endif > Inactive
	</div>
	<div class="form-group px-5">
        <label for="">Image</label>
		<input type="file" class="form-control" name="image_name[]"  multiple="multiple" {{isset($user) ? ' ' : 'required'}}>
		@if (isset($user))
			 @foreach ($user->image as $image )
				 <img src="{{asset('public/storage/uploads/product/thumb/'.$image->image_name)}}" alt="Product-Image">
				 <input type="radio" name="check"
				 @if ($image->status== 1) 
					 {{"checked"}}
					 @else{{" "}}
				@endif value="{{$image->id}}">
			 @endforeach
		@endif
    </div>
    <div class="form-group px-5">
        <button class="btn bg-primary" id="submit" name="submit">Submit</button>
    </div>
   {{-- @php  $user=auth()->user();
    print_r($user);
    echo $user->id;
    @endphp --}}
</form>
</div>
<script>
//$(document).ready(function(){
$("#productForm").validate({
    rules: {
             product_name: {
				required: true
			},
			category_id: {
				required: true
			},
			product_code: {
				required: true
			},
			price: {
				required: true,
				digits: true
			},
			sale_price: {
				required: true,
				digits: true
			},
			quantity: {
				required: true,
				digits: true
			},
			product_order: {
				required: true,
				digits: true
			},
			image_name: {
				required: true,
				extension: "jpg|jpeg|png|gif"
			},
			status: {
				required: true,
			}
		},
		messages: {
			product_name: {
				required: "Please Enter Product Name"
			},
			category_id: {
				required: "Please Select Category"
			},
			product_code: {
				required: "Please Enter Product code"
			},
			price: {
				required: "Please Enter Price",
				digits: "Please Enter Only Digits"

			},
			sale_price: {
				required: "Please Enter Sales Price",
				digits: "Please Enter Only Digits"

			},
			quantity: {
				required: "Please Enter Quantity",
				digits: "Please Enter Only Digits"
			},
			product_order: {
				required: "Please Enter Order",
				digits: "Please Enter Only Digits"
			},
			image_name: {
				required: "Please select image",
				extension: "jpg|jpeg|png|gif"
			},
			status: {
				required: "Please select status",
			}
		},
		errorPlacement: function(error, element) {
			if (element.attr("type") == "radio") {
				error.insertAfter($(element).parent('div')).css({
					"padding-left": "47px"
				});
			} else {
				error.insertAfter($(element));
			}
		},
	});
</script>
@endsection