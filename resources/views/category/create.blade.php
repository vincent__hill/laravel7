    @extends('layouts.layoutPage') 
    @section('content')
    <div class="container py-3 my-5 bg-light border">
        <h1 class="text-center">Category</h1>
    <form action="{{url(isset($user) ? 'categories/'.$user->id : '/categories')}}"  method="POST"   enctype="multipart/form-data" id="CategoryForm">
            @csrf
            @if (isset($user))
                @method('PUT')
            @endif
            <div class="form-group px-5">
                <label for="">Name</label>
                <input type="text" class="form-control" name="category_name" value="{{isset($user) ? $user->category_name : ''}}" >
            </div>
            <div class="form-group px-5">
                <label for="">Image</label>
                <input type="file" class="form-control" name="category_image" {{isset($user) ? ' ' : 'required'}} >
                @if (isset($user))
            <img src="{{asset('public/storage/uploads/category/thumb/'.$user->category_image)}}" alt="Category-Image">

                @endif
            </div>
            <div class="form-group px-5">
                <label for="">Order</label>
                <input type="number" class="form-control" name="ordering" value="{{isset($user) ? $user->ordering : ''}}">
            </div>
            <div class="form-group px-5">
                <label for="">Status</label>
                <input type="radio"  name="status" value="1" @if (isset($user))
                                                                @if ($user->status==1)
                                                                {{"checked"}}
                                                                @endif
                                                                @endif> Active
                <input type="radio"  name="status" value="0" @if (isset($user))
                                                                @if ($user->status==0)
                                                                {{"checked"}}
                                                                @endif
                                                                 @endif > Inactive
            </div>
            <div class="form-group px-5">
                <button class="btn bg-primary" id="submit" name="submit">Submit</button>
            </div>
        </form>
    </div>
    <script>
        $("#CategoryForm").validate({
            rules: {
                category_name: {
                          required: true
                     },
                ordering: {
                    required: true
                },
                status: {
                    required: true
                }
            },
            messages: {
                category_name: {
                    required: "Please Enter Name"
                },
                ordering: {
                    required: "Please Enter Order"
                },
                status: {
                    required: "Please select status"
                },
            },
            errorPlacement: function(error, element) {
                if (element.attr("type") == "radio") {
                    error.insertAfter($(element).parent('div')).css({
                        "padding-left": "46px"
                    });
                } else {
                    error.insertAfter($(element));
                }
            }
        });
    </script>
 @endsection