    {{-- @if (Auth::check()) --}}
    @extends('layouts.layoutPage') 
    @section('content')
       {{--  {{ $user=auth()->user()->id}}  --}}
    {{-- @else
        <script type="text/javascript">
            window.location ="{{ url('/login')}}";//here double curly bracket
        </script>
    @endif
    @php
        // $categories = \App\Models\Category::with('product')->first();
        // echo $categories->product->product_name;
    @endphp --}}
    <h1 class="text-center">Category</h1>
    <div class="px-5 pt-2">
	    <input type="text" name="search" id="search" class="form-control" placeholder="search...">
    </div>
    <div class="pr-3 my-4">
        <table class="table table-striped" id="table">
            
            <thead>
                <tr class="bg-secondary">
                    <th>NO.</th>
				    <th>Image</th>
                    <th class="category_name">
                        <div style="display: inline-flex;">Name
                            <button class="bg-secondary" data-id='asc' name="category_name" value="category_name" onclick="sort('category_name','ASC')">
                                <i class="fas fa-long-arrow-alt-up "></i>
                            </button>
                            <button class="bg-secondary" data-id="desc" name="category_name" value="category_name" onclick="sort('category_name','DESC')">
                                <i class="fas fa-long-arrow-alt-down"></i>
                            </button>
                        </div>
                    </th>
                    <th>Order</th>
                    <th>No of product</th>
                    <th>Added</th>
                    <th>modified</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                   // print_r($values);
                    // exit();
                    $path='/public/storage/uploads/category/thumb/';
                @endphp
                 @if (count($values)>0)
                    @foreach ($values as $key=>$value )
                    {{--  $imageName=$path.$value->category_image;  --}}
                    <tr>
                        <td>#</td>
                        <td><img src="{{ asset($path.$value->category_image)}}" alt="Category-Image"></td>
                        <td>{{$value->category_name}}</td>
                        <td>{{$value->ordering}}</td>
                        <td>{{$value->products()->count()}}</td>
                        <td>{{$value->created_at}}</td>
                        <td>{{$value->updated_at}}</td>
                        <td>
                            @if ($value->status==1)
                                <button class="btn-success" id="active" >Active</button>
                            @else
                                <button class="btn-warning" id="inactive">Inactive</button>
                            @endif
                        </td>
                        <td>
                        <a href="{{ url('categories/'.$value->id.'/edit')}}"><button class="btn">Edit</button></a>
                        {{-- <a href="{{ url('categories/'.$value->id)}}"> <button class="btn" id="delete" onclick="return confirmDelete()">Delete</button></a> --}}
                        <form action="{{ url('categories/'.$value->id)}}" method="post">
                            @csrf
                            {{ method_field('delete') }}

                            <button class="btn btn-default" onclick="return confirmDelete()">Delete</button>
                        </form>
                        </td>
                    </tr>
                    @endforeach
                 @else
                    <tr>
                        <td colspan="12" class="text-center error"> Data is not Available!!</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <script>
            $(document).ready(function(){
                $("#search").on("keyup",function(){
                    var value = $(this).val().toLowerCase();
                    var userId = "{{$user=auth()->user()->id}}";
                    $.ajax({
                        url:'{{route("sorting.post")}}',
                        type:'POST',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            'value': value,
                            'userId': userId
                        },
                        success:function(data){
                            //alert(data);
                            console.log(data);
                            $(".table tr:not(:first)").remove();
						$(".table").append(data);
                    //         if (!data) {
					// 	//$("#msg").text("Data is not Available");
					// 	$(".table tr:not(:first)").remove();
					// } else {
					// 	//$("#msg").text(" ");
					// 	$(".table tr:not(:first)").remove();
					// 	$(".table").append(data);
					// }
                        }
                    });
                });
            });
                function sort(column,sort){
                    var userId = "{{$user=auth()->user()->id}}";
                   // alert("lsdals");
                    $.ajax({
                        url:'{{route("sorting.post")}}',
                        type: 'POST',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "column": column,
                            "sort": sort,
                            "userId": userId
                        },
                        success: function(data) {
                            //alert(data);
                            console.log(data);
                            $(".table tr:not(:first)").remove();
                            $(".table").append(data);

                        }
	                	});
                }
                function confirmDelete() {
		//alert("dsfl;kld");
                    if (confirm("Do You Want Delete ??") == true) {
                        return true;
                    } else {
                        // alert("Cancel by user");
                        return false;
                    }
	            }
    </script>
 @endsection


