@php
// print_r($values);
// exit();
  //  $path='../laravel7/resources/views/uploads/Category/';

@endphp
@if (count($values)>0)
        @foreach ($values as $value )
        <tr>
            <td>#</td>
            <td><img src="{{url('/public/storage/uploads/category/thumb/'.$value->category_image)}}" alt="Category-Image" width=100 height=100></td>
            <td>{{$value->category_name}}</td>
            <td>{{$value->ordering}}</td>
            <td>{{$value->products()->count()}}</td>
            <td>{{$value->created_at}}</td>
            <td>{{$value->updated_at}}</td>
            <td>
                @if ($value->status==1)
                    <button class="btn-success" id="active" >Active</button>
                @else
                    <button class="btn-warning" id="inactive">Inactive</button>
                @endif
            </td>
            <td>
                <a href="{{ url('categories/'.$value->id.'/edit')}}"><button class="btn">Edit</button></a>
                        {{-- <a href="{{ url('categories/'.$value->id)}}"> <button class="btn" id="delete" onclick="return confirmDelete()">Delete</button></a> --}}
                        <form action="{{ url('categories/'.$value->id)}}" method="post">
                            @csrf
                            {{ method_field('delete') }}

                            <button class="btn btn-default" onclick="return confirmDelete()">Delete</button>
                        </form>
            </td>
        </tr>
        @endforeach
@else
        <tr>
            <td colspan="12" class="text-center error">Data is not available</td>
        </tr>
@endif