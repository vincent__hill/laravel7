<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset('/public/css/bootstrap.min.css')}}">
    <style>
        .error{
            color:red;
        }
    </style>
</head>
<body>
    <div class="container my-5 py-4 px-5 border bg-light">
        <h1 class="text-center">Login</h1>
        @if($errors->any())
        <div class="alert alert-warning px-5">
            <strong>Warning!</strong> {{$errors->first()}}.
          </div>
         @endif
        <form action="login-check" method="POST" id="loginForm">
            @csrf
            <div class="form-group px-5">
                <label for="">Email</label>
                <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group px-5">
                <label for="">Password</label>
                <input type="password" name="password" class="form-control">
            </div>
            <div>
                <button class="btn btn-primary pr-4" style="float:right">Submit</button>
                <a href="registration">SignUp</a>
            </div>
        </form>
    </div>
    <script src="{{asset('/public/js/jquery-3.5.1.min.js')}}"></script>
    <script src="{{asset('/public/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('/public/js/additional-methods.min.js')}}"></script>
    <script>
        $("#loginForm").validate({
            rules:{
                email:{
                    required:true,
                    email:true
                },
                password:{
                    required:true,
                    minlength:6
                }
            },
            messages:{
                email:{
                    required:"Please enter email",
                    email:"Please enter valid email"
                },
                password:{
                    required:"Please enter password",
                    minlength:"Please enter minimum 6 digit"
                }
            }
        });
    </script>
</body>
</html>